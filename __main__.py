import linecache as lc

ordinal = ['first','second','third','fourth','fifth','sixth','seventh','eighth','ninth','tenth','eleventh','twelvth']
digit = ['a','two','three','four','five','six','seven','eight','nine','ten','eleven','twelve']

for i in range(12):
    verse = 'On the %s day of Christmas\nmy true love gave to me:\n' % ordinal[i]
    for j in reversed(range(0, i + 1)):
        if i > 0 and j == 0:
            verse += 'and '
        verse += digit[j] + ' ' + lc.getline('input.txt', j + 1)

    print(verse)
    print('\n')

