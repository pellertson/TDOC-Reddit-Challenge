#The Twelve Days of Christmas

So just for fun, I decided to do [this challenge from
/r/dailyprogrammer](https://redd.it/5j6ggm). In this challenge, you print out
the lyrics from the Twelve Days of Christmas with no input. The challenge said
to do it in as few of lines as possible, and I got it down to 14. But I saw
other people get it down to **three**. This is my first challenge I have done
off of this subreddit, but it definetly won't be my last!
